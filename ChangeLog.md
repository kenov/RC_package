# RC_package

A complete package of RIAM-Compact.


## REVISION HISTORY

---
- 2018-11-22  Version 2.5.0
- CBrick-1.3.0
- RCH-2.7.2


---
- 2018-10-07  Version 2.4.1
- RCH-2.5.9


---
- 2018-10-07  Version 2.4.0
- PMlib-6.3.1
- RCH-2.5.5


---
- 2018-10-06  Version 2.3.9
- RCH-2.5.4


---
- 2018-10-04  Version 2.3.8
- RCH-2.5.2


---
- 2018-10-04  Version 2.3.7
- RCH-2.5.1


---
- 2018-10-04  Version 2.3.6
- RCH-2.5.0


---
- 2018-10-03  Version 2.3.5
- RCH-2.4.7


---
- 2018-10-03  Version 2.3.4
- RCH-2.4.6
- drop CDMlib


---
- 2018-10-02  Version 2.3.3
- RCH-2.4.1
- PMlib-6.3.0
- CBrick-1.1.0


---
- 2018-09-26  Version 2.3.2
- RCH-2.2.9


---
- 2018-09-25  Version 2.3.1
- RCH-2.2.8


---
- 2018-09-25  Version 2.3.0
- RCH-2.2.7
- CBrick-1.0.6
- TextParser-1.8.8
- Polylib-3.7.2
- PMlib-6.2.3


---
- 2018-09-24  Version 2.2.5
- CBrick-1.0.4
- TextParser-1.8.7
- PMlib-6.2.2
- Polylib-3.7.1


---
- 2018-09-24  Version 2.2.4
- RCH-2.2.6


---
- 2018-09-22  Version 2.2.3
- README.md


---
- 2018-09-21  Version 2.2.2
- RCH-2.2.5

---
- 2018-09-20  Version 2.2.1
- install.sh, PMlib -Denable_PreciseTimer=yes, -Denable_OPENMP=yes


---
- 2018-09-19  Version 2.2.0
- RCH-2.2.4


---
- 2018-09-18  Version 2.1.9
- PMlib-6.2.0
- RCH-2.2.3


---
- 2018-09-12  Version 2.1.8
- CBrick-1.0.3
- RCH-2.2.2


---
- 2018-09-10  Version 2.1.7
- CBrick-1.0.2
- RCH-2.2.1


---
- 2018-09-09  Version 2.1.6
- インストール先をRCHに変更
- RCH-2.2.0


---
- 2018-09-07  Version 2.1.5
- RCH-2.1.9


---
- 2018-09-06  Version 2.1.2 ~ 2.1.4
- RCH-2.1.8


---
- 2018-06-17  Version 2.1.1
- RCH-2.1.5


---
- 2018-06-03  Version 2.1.0
- RCH-2.1.3


---
- 2018-04-17  Version 2.0.0
  - RIAM-Compact-2.0.8
  - CBrick-1.0.1
  - PMlib-5.8.5
  - Polylib-3.7.0
  - TextParser-1.8.6


---
- 2017-08-31  Version 1.2.5
  - RIAM-Compact-1.3.1
  - CBrick-0.9.3
  - PMlib-5.7.0


---
- 2017-08-30  Version 1.2.4
  - RIAM-Compact-1.2.9
  - CBrick-0.9.2


---
- 2017-08-30  Version 1.2.3
  - RIAM-Compact-1.2.8
  - CBrick-0.9.1


---
- 2017-08-29  Version 1.2.2
  - RIAM-Compact-1.2.7
  - PMlib-5.6.8


---
- 2017-08-24  Version 1.2.1
  - RIAM-Compact-1.2.6


---
- 2017-08-24  Version 1.2.0
  - RIAM-Compact-1.2.5
  - Polylib-3.6.8


---
- 2017-08-24  Version 1.1.9
  - RIAM-Compact-1.2.4


---
- 2017-08-24  Version 1.1.8
  - RIAM-Compact-1.2.3
  - CBrick-0.9.0


---
- 2017-08-23  Version 1.1.7
  - RIAM-Compact-1.2.2
  - Polylib-3.6.7


---
- 2017-08-10  Version 1.1.6
  - RIAM-Compact-1.1.5
  - PMlib-5.6.7
  - CBrick-0.8.0


---
- 2017-07-07  Version 1.1.5
  - RIAM-Compact-1.0.10
  - TextParser-1.8.5
  - PMlib-5.6.6
  - CPMlib-2.4.4
  - CDMlib-1.1.3


---
- 2017-06-28  Version 1.1.4
  - RIAM-Compact-1.0.9


---
- 2017-06-27  Version 1.1.3
  - RIAM-Compact-1.0.8


---
- 2017-06-26  Version 1.1.2
  - PMlib-5.6.5
  - RIAM-Compact-1.0.7


---
- 2017-06-02  Version 1.1.1
  - CPMlib-2.4.3
  - RIAM-Compact-1.0.6


---
- 2017-06-02  Version 1.1.0
  - TextParser-1.8.4
  - PMlib-5.6.3
  - CPMlib-2.4.2
  - CDMlib-1.1.2
  - RIAM-Compact-1.0.3


---
- 2017-05-25  Version 1.0.10
  - RIAM-Compact-0.9.7


---
- 2017-03-30  Version 1.0.9
  - TextParser-1.8.2
  - PMlib-5.6.1
  - CPMlib-2.4.0
  - CDMlib-1.0.9
  - RIAM-Compact-0.9.6


---
- 2017-02-25  Version 1.0.8
  - PMlib-5.5.9
  - CPMlib-2.3.4
  - CDMlib-1.0.7
  - RIAM-Compact-0.8.12


---
- 2017-02-15  Version 1.0.7
  - CPMlib-2.3.3


---
- 2017-02-15  Version 1.0.6
  - TextParser-1.8.1
  - PMlib-5.5.7
  - CPMlib-2.3.2
  - CDMlib-1.0.6
  - RIAM-Compact-0.8.1
  - Tested.

  |Environment|Serial|MPI |
  |:--|:--:|:--:|
  |Intel / Intel 17.0.1 ||ok|
  |Intel / GNU 6.2.0    ||ok|
  |Fujitsu / fx10       ||ok|
  |Fujitsu / fx100      ||ok|
  |Fujitsu / K          |||


---
- 2017-02-15. Verion 1.0.5
  - update to CPMlib-2.2.3


---
- 2017-02-13. Vesion 1.0.4
  - update to RIAM-Compact-0.7.6
  - update install.sh


---
- 2017-02-12. Version 1.0.3
  - update to RIAM-Compact-0.7.5


---
- 2017-02-12. Version 1.0.2
  - update fx10 compiler options
  - modify `install.sh`.


---
- 2017-02-12. Version 1.0.1
  - modify `install.sh`.


---
- 2017-02-12  Version 1.0.0
  - Cmake version
  - Install shell scripts are integrated into `install.sh`.


---
- 2017-01-27. Verion 0.8.0
  - TextParser-1.7.3
  - RIAM-Compact-0.7.2
  - Modify install_*.sh
  - add ChangeLog.md


---
- 2017-01-26. Version 0.7.1
  - modify compiler options for fx10


---
- 2017-01-23. Version 0.7.0
  - Cmake version

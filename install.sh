#! /bin/sh
#
##############################################################################
#
# RIAM-Compact HPC version
#
# Copyright (C) 2015-2018 Research Institute for Applied Mechanics(RIAM)
#                       / Research Institute for Information Technology(RIIT), Kyushu University.
# All rights reserved.
#
# Copyright (c) 2015-2016 Advanced Institute for Computational Science, RIKEN.
# All rights reserved.
#

#######################################
#
# Library version
#

export TP_LIB=TextParser-1.8.8
export PM_LIB=PMlib-6.3.1
export PL_LIB=Polylib-3.7.2
export CBR_LIB=CBrick-1.3.0
export RCH=RCH-2.7.2



#######################################
#
# Usage
#
# $ ./install.sh <intel|fx10|K|intel_F_TCS> <INST_DIR> {serial|mpi} {double|float} {papi=on|papi=off}
#
#######################################


# Target machine
#
target_arch=$1

if [ "${target_arch}" = "intel" ]; then
  echo "Target arch       : intel"
elif [ "${target_arch}" = "fx10" ]; then
  echo "Target arch       : fx10"
  F_TCS="yes"
  toolchain_file="../cmake/Toolchain_fx10.cmake"
elif [ "${target_arch}" = "K" ]; then
  echo "Target arch       : K Computer"
  F_TCS="yes"
  toolchain_file="../cmake/Toolchain_K.cmake"
elif [ "${target_arch}" = "intel_F_TCS" ]; then
  echo "Target arch       : Intel Fujitsu TCS env."
  toolchain_file="../cmake/Toolchain_intel_F_TCS.cmake"
  F_TCS="yes"
else
  echo "Target arch       : not supported -- terminate install process"
  exit
fi


# Install directory
#
target_dir=$2

export INST_DIR=${target_dir}
echo "Install directory : ${INST_DIR}"


# Parallelism
#
parallel_mode=$3

if [ "${parallel_mode}" = "mpi" ]; then
  echo "Paralle mode.     : ${parallel_mode}"
  mpi_sw="yes"
elif [ "${parallel_mode}" = "serial" ]; then
  echo "Parallel mode.     : ${parallel_mode}"
  mpi_sw="no"
else
  echo "Parallel mode.     : Invalid -- terminate install process"
  exit
fi


# Floating point Precision
#
fp_mode=$4

if [ "${fp_mode}" = "double" ]; then
  export PRCSN=double
elif [ "${fp_mode}" = "float" ]; then
  export PRCSN=float
else
  echo "Invalid argument for floating point  -- terminate install process"
  exit
fi

echo "Precision         : ${fp_mode}"
echo " "


# PAPI
#
hwpc=$5

if [ "${hwpc}" = "papi=on" ]; then
  export PAPI=${INST_DIR}/PAPI
elif [ "${hwpc}" = "papi=off" ]; then
  export PAPI=OFF
else
  echo "Invalid argument for papi  -- terminate install process"
  exit
fi

echo "PAPI              : ${hwpc}"
echo " "


#######################################


# TextParser
#
echo
echo -----------------------------
echo Install TextParser
echo
if [ ! -d ${TP_LIB} ]; then
  tar xvzf ${TP_LIB}.tar.gz
  mkdir ${TP_LIB}/build
  cd ${TP_LIB}/build

  if [ "${target_arch}" = "intel" ]; then
      cmake -DINSTALL_DIR=${INST_DIR}/TextParser \
            -Dwith_MPI=${mpi_sw} \
            -Denable_fapi=no \
            -Denable_test=no ..

  elif [ "${F_TCS}" = "yes" ]; then
      cmake -DINSTALL_DIR=${INST_DIR}/TextParser \
            -DCMAKE_TOOLCHAIN_FILE="${toolchain_file}" \
            -Dwith_MPI=${mpi_sw} \
            -Denable_fapi=no \
            -Denable_test=no ..
  fi

  make
  if [ $? -ne 0 ]; then
    echo "make error!"
    exit
  fi
  make install
  cd ../..
else
  echo "${TP_LIB} is already exist. Skip compiling."
fi



# PMlib
#
echo
echo -----------------------------
echo Install PMlib
echo
if [ ! -d ${PM_LIB} ]; then
  tar xvzf ${PM_LIB}.tar.gz
  mkdir ${PM_LIB}/build
  cd ${PM_LIB}/build

  if [ "${target_arch}" = "intel" ]; then
      cmake -DINSTALL_DIR=${INST_DIR}/PMlib \
            -Denable_OPENMP=yes \
            -Dwith_MPI=${mpi_sw} \
            -Denable_Fortran=no \
            -Dwith_example=no \
            -Dwith_PAPI=${PAPI} \
            -Dwith_OTF=no \
            -Denable_PreciseTimer=yes ..

  elif [ "${F_TCS}" = "yes" ]; then
      cmake -DINSTALL_DIR=${INST_DIR}/PMlib \
            -DCMAKE_TOOLCHAIN_FILE="${toolchain_file}" \
            -Denable_OPENMP=yes \
            -Dwith_MPI=${mpi_sw} \
            -Denable_Fortran=no \
            -Dwith_example=no \
            -Dwith_PAPI=${PAPI} \
            -Dwith_OTF=no \
            -Denable_PreciseTimer=yes ..
  fi

  make
  if [ $? -ne 0 ]; then
    echo "make error!"
    exit
  fi
  make install
  cd ../..
else
  echo "${PM_LIB} is already exist. Skip compiling."
fi



# CBrick
#

if [ "${mpi_sw}" = "yes" ]; then

echo
echo -----------------------------
echo Install CBrick
echo
if [ ! -d ${CBR_LIB} ]; then
  tar xvzf ${CBR_LIB}.tar.gz
  mkdir ${CBR_LIB}/build
  cd ${CBR_LIB}/build

  if [ "${target_arch}" = "intel" ]; then
      cmake -DINSTALL_DIR=${INST_DIR}/CBrick \
            -Denable_OPENMP=yes \
            -Dwith_example=no \
            -Dwith_Diagonal=yes ..

  elif [ "${F_TCS}" = "yes" ]; then
      cmake -DINSTALL_DIR=${INST_DIR}/CBrick \
            -DCMAKE_TOOLCHAIN_FILE="${toolchain_file}" \
            -Denable_OPENMP=yes \
            -Dwith_example=no \
            -Dwith_Diagonal=yes ..
  fi

  make
  if [ $? -ne 0 ]; then
    echo "make error!"
    exit
  fi
  make install
  cd ../..
else
  echo "${CBR_LIB} is already exist. Skip compiling."
fi

fi




# Polylib
#
echo
echo -----------------------------
echo Polylib
echo
if [ ! -d ${PL_LIB} ]; then
  tar xvzf ${PL_LIB}.tar.gz
  mkdir ${PL_LIB}/build
  cd ${PL_LIB}/build

  if [ "${target_arch}" = "intel" ]; then
    cmake -DINSTALL_DIR=${INST_DIR}/Polylib \
          -Dreal_type=${PRCSN} \
          -Dwith_MPI=${mpi_sw} \
          -Dwith_example=no \
          -Dwith_TP=${INST_DIR}/TextParser ..

  elif [ "${F_TCS}" = "yes" ]; then
    cmake -DINSTALL_DIR=${INST_DIR}/Polylib \
          -DCMAKE_TOOLCHAIN_FILE="${toolchain_file}" \
          -Dreal_type=${PRCSN} \
          -Dwith_MPI=${mpi_sw} \
          -Dwith_example=no \
          -Dwith_TP=${INST_DIR}/TextParser ..
  fi

  make
  if [ $? -ne 0 ]; then
    echo "make error!"
    exit
  fi
  make install
  cd ../..
else
  echo "${PL_LIB} is already exist. Skip compiling."
fi



# RCH
#

echo
echo -----------------------------
echo Install RIAM-COMPACT HPC version
echo
if [ ! -d ${RCH} ]; then
  tar xvzf ${RCH}.tar.gz
  mkdir ${RCH}/build
  cd ${RCH}/build

  if [ "${target_arch}" = "intel" ]; then
    cmake -DINSTALL_DIR=${INST_DIR}/RCH \
          -Dreal_type=${PRCSN} \
          -Denable_OPENMP=yes \
          -Dwith_MPI=${mpi_sw} \
          -Dwith_TP=${INST_DIR}/TextParser \
          -Dwith_PM=${INST_DIR}/PMlib \
          -Dwith_PAPI=${PAPI} \
          -Dwith_PL=${INST_DIR}/Polylib \
          -Dwith_CBR=${INST_DIR}/CBrick \
          -Dwith_CDM=OFF \
          -Dwith_HDF=OFF ..

  elif [ "${F_TCS}" = "yes" ]; then
    cmake -DINSTALL_DIR=${INST_DIR}/RCH \
          -DCMAKE_TOOLCHAIN_FILE="${toolchain_file}" \
          -Dreal_type=${PRCSN} \
          -Denable_OPENMP=yes \
          -Dwith_MPI=${mpi_sw} \
          -Dwith_TP=${INST_DIR}/TextParser \
          -Dwith_PM=${INST_DIR}/PMlib \
          -Dwith_PAPI=${PAPI} \
          -Dwith_PL=${INST_DIR}/Polylib \
          -Dwith_CBR=${INST_DIR}/CBrick \
          -Dwith_CDM=OFF \
          -Dwith_HDF=OFF ..
  fi

  make
  if [ $? -ne 0 ]; then
    echo "make error!"
    exit
  fi
  make install
  cd ../..
else
  echo "${RCH} is already exist. Skip compiling."
fi

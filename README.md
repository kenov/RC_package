# RC_package

Release package of RIAM-Compact and related libraries.

Riam Compact is the acronym standing for Research Institute for Applied Mechanics, Kyushu University, Computational Prediction of Airflow over Complex Terrain, and is a numerical-CFD-model/software-package which has an object domain from several m to several km, and can predict the airflow and the gas diffusion over complex terrain with high precision.


## Copyright
- Copyright (c) 2015-2018 Research Institute for Applied Mechanics(RIAM), Kyushu University. All rights reserved.
- Copyright (c) 2016-2018 Research Institute for Information Technology(RIIT), Kyushu University. All rights reserved.
- Copyright (c) 2015-2016 Advanced Institute for Computational Science(AICS), RIKEN. All rights reserved.


## Prerequisite

- Cmake
- MPI library
- TextParser (included in this package)
- PMlib (included in this package)
- CBrick (included in this package)
- Polylib (included in this package)
- PAPI (option)


## Install
Type install shell script on command line with options .

~~~
$ export CC=... CXX=... F90=... FC=...
$ ./install.sh <intel|fx10|K|intel_F_TCS> <INST_DIR> {serial|mpi} {double|float} papi={on|off}
~~~


### Mac Intel compiler + OpenMPI
~~~
$ export CC=mpicc CXX=mpicxx F90=mpif90 FC=mpif90
$ ./install.sh intel ${HOME}/RC mpi float papi=off
~~~


### ITO system

#### Intel compiler /w PAPI

~~~
$ module load intel/2018.3 openmpi/2.1.3-nocuda-intel18.0
~~~

##### PAPI (papi-5.6.0)
~~~
$ ./configure --prefix=${HOME}/RC/PAPI CC=icc F77=ifort
$ make
$ make install
~~~

##### RCH
~~~
$ export CC=mpiicc CXX=mpiicpc F90=mpiifort FC=mpiifort
$ ./install.sh intel ${HOME}/RC mpi float papi=on
~~~


#### Fujitsu compiler TCS env. /wo PAPI

##### PAPI (papi-5.6.0)
~~~
$ export CC=mpifcc CXX=mpiFCC F77=mpifrt CFLAGS=-Xg CXXFLAGS=-Xg
$ ./configure --prefix=${HOME}/RC-fj/PAPI
$ make
$ make install
~~~

##### RCH
~~~
$ ./install.sh intel_F_TCS ${HOME}/RC-fj mpi float papi=on
~~~


## Execution
~~~
$ export HWPC_CHOOSER={FLOPS | BANDWIDTH}
$ mpirun -np 4 ./rch-mpi hoge.tp
~~~


## Supported platforms and compilers

* Linux Intel arch., Intel/GNU/PGI compilers.
* Mac OSX, Intel/GNU/PGI compilers.
* Sparc fx arch. and Fujitsu compiler.



## Contributors

- Takanori Uchida
- Kenji Ono *keno@cc.kyushu-u.ac.jp*


